#!/usr/bin/env python3

from distutils.core import setup
from sorcnote.commons import Commons

setup(name=Commons.app_name,
      version=Commons.app_ver,
      description=Commons.app_desc,
      author='Sorcerer',
      author_email='sorcerersr@mailbox.org',
      url='https://gitlab.com/sorcerersr/sorcnote',
      packages=['sorcnote'],
      )
