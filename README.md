# **Sorc**erers **Note** Application (short: SorcNote)

Take and read markup notes.

## License

This software may be modified and distributed under the terms
of the GNU General Public License version 3. 
See the [LICENSE file](LICENSE) for details.

## Dependecies

    * Python 3
    * Gtk3 (+ GtkWebView and GtkSourceView) 
    * PyGObject (Python bindings for GTK3)


## Style schemes for edit mode

included by GtkSourceView:

        'classic', 'cobalt', 'kate', 'oblivion', 'solarized-dark', 'solarized-light', 'tango'

additional default theme included with sorcenote:

        'monokai-extended' from https://gist.github.com/LeoIannacone/71028cc3bce04567d77e

Third party style-schemes from https://wiki.gnome.org/Projects/GtkSourceView/StyleSchemes
can be used by placing any of them in the default search path of GtkSourceView or
in the 'syntaxstyles' directory of sorcnote and then set the value of 
the config key 'sourceview_scheme' to the id of the style scheme.

