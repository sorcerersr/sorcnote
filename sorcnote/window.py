#!/bin/env python3
#
#               This file is part of SorcNote.
#
#         Copyright (C) 2017 sorcerersr@mailbox.org
#
# SorcNote is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SorcNote is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#
"""
The main window of the application
"""
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GtkSource', '3.0')
try:
    gi.require_version('WebKit2', '4.0')
    from webkit2wrapper import WebViewWrapper  # noqa: 402
except ValueError:
    gi.require_version('WebKit', '3.0')
    from webkitwrapper import WebViewWrapper  # noqa: 402
from gi.repository import Gtk  # noqa: E402
from gi.repository import GtkSource  # noqa: E402
from gi.repository.Gdk import EventType  # noqa: E402
from gi.repository.GdkPixbuf import Pixbuf, PixbufLoader, \
 InterpType  # noqa: E402
from config import Key  # noqa: E402
from dialogs import AddDatabaseDialog, AddCategoryDialog  # noqa: E402
from menus import CategoryContextMenu, NoteContextMenu  # noqa: E402
from dialogs import AboutDialog, SettingsDialog  # noqa: E402
from gi.repository.Pango import FontDescription  # noqa: E402


BASE_SIZE_DB_LABEL = 10000


class DatabaseView(Gtk.Box):
    """
    Helper class for structuring the display of a database.

    To display a database use a label to dispay its name, list the categories
    and provide a button to add a new category (and a way to delete a category)
    """

    def __init__(self, name, categories, controller, parent, config):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=6)

        self.parent = parent

        self.controller = controller

        inner_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.pack_start(inner_box, False, True, 0)

        label_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)

        size = BASE_SIZE_DB_LABEL * config[Key.ZOOM_FACTOR]

        self.name = name
        label = Gtk.Label('<span size="' + str(int(size)) +
                          '"><b>' + name + '</b></span>')

        label.props.use_markup = True
        label_box.pack_start(label, True, True, 0)

        add_button = Gtk.Button(None, image=Gtk.Image(stock=Gtk.STOCK_ADD))
        add_button.connect("clicked", self.add_new_category_clicked)
        label_box.pack_start(add_button, False, True, 0)

        inner_box.pack_start(label_box, False, True, 0)

        self.cat_store = Gtk.ListStore(Pixbuf, str, int)

        self.listview = Gtk.TreeView(self.cat_store)
        self.listview.set_headers_visible(False)
        self.listview.connect("button_press_event", self.handle_mouse_click)

        pix_renderer = Gtk.CellRendererPixbuf()
        icon_column = Gtk.TreeViewColumn("Icon", pix_renderer, pixbuf=0)
        self.listview.append_column(icon_column)

        self.cat_renderer = Gtk.CellRendererText()
        self.cat_renderer.props.scale = config[Key.ZOOM_FACTOR]
        column = Gtk.TreeViewColumn("Category", self.cat_renderer, text=1)
        self.listview.append_column(column)

        inner_box.pack_start(self.listview, False, True, 0)

        self.apply_categories(categories)

        selection = self.listview.get_selection()
        selection.connect("changed", self.selection_changed)

    def apply_categories(self, categories):
        """
        Apply categories to the liststore.
        """
        print("apply categories")
        self.cat_store.clear()
        for cat in categories:
            pixbuf = None
            if cat.image is not None:
                loader = PixbufLoader.new_with_type('png')
                loader.write(cat.image)
                pixbuf = loader.get_pixbuf()
                pixbuf = pixbuf.scale_simple(24, 24, InterpType.BILINEAR)
                loader.close()
            self.cat_store.append([pixbuf, cat.name, cat.id])

    def selection_changed(self, selection):
        """
        Callback for category selection.
        Delegates the call to the corresponding method of the controller.
        """
        treeModel, treeIter = selection.get_selected()
        if treeIter is not None:
            self.controller.category_selected(
                self.name, treeModel[treeIter][2])

    def reset_selection(self):
        """
        Reset all selections of the containing listview.
        """
        selection = self.listview.get_selection()
        selection.unselect_all()

    def add_new_category_clicked(self, button):
        """
        Callback for the "add new category" button
        """
        dialog = AddCategoryDialog(self.parent)
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            cat_name = dialog.get_cat_name()
            file_name = dialog.get_cat_image()
            self.controller.add_new_category(self.name, cat_name, file_name)

        dialog.destroy()

    def handle_mouse_click(self, treeview, event):
        """
        Handle mouse button clicks on the categories treeview.
        Used to show a popup menu when using a right click.
        """
        if event.button == 3:
            selection = self.listview.get_selection()
            (treemodel, treeiter) = selection.get_selected()
            if treeiter is not None:
                print(treemodel[treeiter][2])
                menu = CategoryContextMenu(self.parent,
                                           self.controller,
                                           self.name,
                                           treemodel[treeiter][2])
                menu.popup(None, None, None, None, event.button, event.time)


class MyWindow(Gtk.Window):

    def __init__(self, commons, config, controller):
        Gtk.Window.__init__(
            self, title=commons.app_name + ' (v.' + commons.app_ver + ')')
        self.controller = controller
        self.apply_size_and_position(config)
        self.config = config

        logo = Pixbuf.new_from_file(self.config.app_root + '/logo/logo.png')
        self.set_default_icon(logo)

        self.connect("delete-event", self.quit)
        self.db_views = {}

        box_outer = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(box_outer)

        box_inner = Gtk.Box()
        box_outer.pack_start(box_inner, True, True, 0)
        # ######################################

        # the categories view
        cat_box = box_spacing = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,
                                        spacing=0)
        box_inner.pack_start(cat_box, False, True, 0)

        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.set_border_width(5)
        scrolled_window.set_policy(
            Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)

        cat_box.pack_start(scrolled_window, True, True, 0)

        self.db_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)

        # outer spacing box
        box_spacing = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL,
                              spacing=5)

        box_spacing.pack_start(self.db_box, True, True, 15)

        scrolled_window.add_with_viewport(box_spacing)

        # below the scrolled window for the dbs and cats
        # display buttons for settings and about dialogs
        button_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        settings_image = Gtk.Image(stock=Gtk.STOCK_PROPERTIES)
        about_image = Gtk.Image(stock=Gtk.STOCK_ABOUT)
        add_image = Gtk.Image(stock=Gtk.STOCK_ADD)

        settings_button = Gtk.Button(None, image=settings_image)
        about_button = Gtk.Button(None, image=about_image)
        add_db_button = Gtk.Button(None, image=add_image)

        settings_button.connect("clicked", self.settings_button_clicked)
        about_button.connect("clicked", self.about_button_clicked)
        add_db_button.connect("clicked", self.add_new_db_clicked)

        button_box.pack_start(settings_button, False, False, 10)
        button_box.pack_start(about_button, False, False, 10)
        button_box.pack_start(add_db_button, False, False, 10)

        cat_box.pack_start(button_box, False, False, 10)

        #####################################################

        note_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)

        scrolled_window_notes = Gtk.ScrolledWindow()
        scrolled_window_notes.set_border_width(5)
        scrolled_window_notes.set_policy(
            Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)

        # outer spacing box
        box_spacing_notes = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL,
                                    spacing=5)

        self.notes_listview = self.__create_note_list()
        self.notes_listview.connect(
            "button_press_event", self.handle_note_mouse_click)

        box_spacing_notes.pack_start(self.notes_listview, True, True, 15)

        scrolled_window_notes.add_with_viewport(box_spacing_notes)

        box_inner.pack_start(note_box, False, True, 0)
        self.add_note_button = Gtk.Button(
            "add new note", image=Gtk.Image(stock=Gtk.STOCK_ADD))

        self.add_note_button.set_sensitive(False)

        self.add_note_button.connect("clicked", self.add_new_note_clicked)

        note_box.pack_start(self.add_note_button, False, True, 10)
        note_box.pack_start(scrolled_window_notes, True, True, 0)

        ######################################################

        self.stack = Gtk.Stack()

        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.set_border_width(5)
        scrolled_window.set_policy(
            Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

        scrolled_window.add_with_viewport(self.stack)

        self.stack.set_transition_type(
            Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
        self.stack.set_transition_duration(250)
        box_inner.pack_start(scrolled_window, True, True, 10)

        self.webview = WebViewWrapper(self.config.app_root)
        self.stack.add_named(self.webview, "display")

        self.buffer_md = GtkSource.Buffer()

        lang_manager = GtkSource.LanguageManager()
        self.buffer_md.set_language(lang_manager.get_language('markdown'))
        self.buffer_md.set_highlight_syntax(True)

        style_manager = GtkSource.StyleSchemeManager.get_default()
        style_manager.append_search_path(
            self.config.app_root + '/syntaxstyles')
        style_manager.force_rescan()
        scheme = style_manager.get_scheme(self.config[Key.SOURCEVIEW_SCHEME])
        self.buffer_md.set_style_scheme(scheme)

        self.sourceview = GtkSource.View.new_with_buffer(self.buffer_md)
        self.sourceview_apply_scale()
        self.sourceview.set_show_line_numbers(True)
        self.sourceview.set_show_line_marks(True)
        self.sourceview.set_highlight_current_line(True)
        self.stack.add_named(self.sourceview, "edit")

        self.webview.connect(
            'context-menu', self.webview_context_menu_callback)
        self.sourceview.connect(
            'populate-popup', self.sourceview_context_menu_callback)

        ######################################################

        scale_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)

        placeholder = Gtk.Label()
        placeholder.set_text("Zoom")
        scale_box.pack_start(placeholder, False, False, 10)

        zoom_store = Gtk.ListStore(float, str)
        zoom_store.append([0.5, "50 %"])
        zoom_store.append([0.75, "75 %"])
        zoom_store.append([1.0, "100 %"])
        zoom_store.append([1.1, "110 %"])
        zoom_store.append([1.25, "125 %"])
        zoom_store.append([1.5, "150%"])
        zoom_store.append([1.75, "175%"])
        zoom_store.append([2.0, "200%"])

        zoom_combo = Gtk.ComboBox.new_with_model(zoom_store)
        # zoom_combo.connect("changed", self.on_name_combo_changed)
        renderer_text = Gtk.CellRendererText()
        zoom_combo.pack_start(renderer_text, True)
        zoom_combo.add_attribute(renderer_text, "text", 1)

        config_factor = self.config[Key.ZOOM_FACTOR]
        iter = zoom_store.get_iter_first()
        while iter is not None:
            zoom_factor = zoom_store.get_value(iter, 0)
            if config_factor == zoom_factor:
                zoom_combo.set_active_iter(iter)
                self.webview.apply_zoom_level(zoom_factor)
                break
            iter = zoom_store.iter_next(iter)

        zoom_combo.connect("changed", self.handle_webview_zoom_change)

        scale_box.pack_start(zoom_combo, False, False, 10)

        cat_box.pack_start(scale_box, False, False, 10)

        ######################################################

        status_bar_box = Gtk.Box()
        box_outer.pack_start(status_bar_box, False, True, 0)

        label = Gtk.Label()
        label.set_text("This will be the statusbar")
        # status_bar_box.pack_start(label, True, True, 0)

        self.note_selection_enabled = True

        self.connect('event', self.generic_window_event)

    def quit(self, window, event):
        """
        Callback for the delete-event.
        Handles user requested programm exit
        """
        self.config.write_config()
        Gtk.main_quit()

    def generic_window_event(self, window, event):

        if event.type == EventType.CONFIGURE:

            self.config[Key.WINDOW_SIZE_WIDTH] = event.width
            self.config[Key.WINDOW_SIZE_HEIGHT] = event.height
            self.config[Key.WINDOW_POS_X] = event.x
            self.config[Key.WINDOW_POS_Y] = event.y

    def sourceview_apply_scale(self):
        size = int(10 * self.config[Key.ZOOM_FACTOR])
        font_desc = FontDescription.from_string('mono ' + str(size))

        self.sourceview.modify_font(font_desc)

    def apply_size_and_position(self, config):
        """
        Restore window size and position (and maximize state) from config.
        """
        if config[Key.WINDOW_MAXIMIZED]:
            self.maximize()
        else:
            self.unmaximize()

        self.set_default_size(
            config[Key.WINDOW_SIZE_WIDTH], config[Key.WINDOW_SIZE_HEIGHT])

        if config[Key.WINDOW_POS_X] and config[Key.WINDOW_POS_Y]:
            self.move(config[Key.WINDOW_POS_X], config[Key.WINDOW_POS_Y])

    def run_main_loop(self):
        """
        Run the UI main loop.
        """
        self.show_all()
        # run the main loop
        Gtk.main()

    def handle_webview_zoom_change(self, combo):
        """
        Callback for the change signal of the webview zoom combobox.
        """
        iter = combo.get_active_iter()
        if iter is not None:
            model = combo.get_model()
            factor = model.get_value(iter, 0)
            self.config[Key.ZOOM_FACTOR] = factor
            self.config.write_config()
            self.webview.set_zoom_level(factor)
            self.note_renderer.props.scale = factor
            for db_name, db in self.db_views.items():
                db.cat_renderer.props.scale = factor
            self.sourceview_apply_scale()

            # the call to hide() followed by the show_all() will cause a
            # re-render of the window and its children with the new sizes
            # applied
            self.hide()
            self.show_all()

    def append_category(self, name, records):
        """
        Append a database with its categories to the first column of the app.
        """
        print("append: " + name)
        db = DatabaseView(name, records, self.controller, self, self.config)
        self.db_views[name] = db
        self.db_box.pack_start(db, False, True, 0)
        db.show()

    def deselect_others(self, db_name=None):
        """
        Reset selections in the listviews for categories. There is a list
        view for each database but only one category over all databases should
        be selectable. This method deselect all previously selected records
        of all list views.
        """
        print("deselect_others ", db_name)
        self.cur_db = db_name
        for name, db in self.db_views.items():
            if not name == db_name:
                db.reset_selection()

    def __create_note_list(self):
        """
        Convenience method for creating the widget structure for displaying
        the list of notes.
        """
        self.note_store = Gtk.ListStore(str, int)
        listview = Gtk.TreeView(self.note_store)
        self.note_renderer = Gtk.CellRendererText()
        self.note_renderer.props.scale = self.config[Key.ZOOM_FACTOR]
        column = Gtk.TreeViewColumn("Notes", self.note_renderer, text=0)
        listview.append_column(column)
        listview.set_headers_visible(False)

        selection = listview.get_selection()
        selection.connect("changed", self.note_selected)

        return listview

    def note_selected(self, selection):
        """
        Callback handling the selection of a note from the note list.
        Will delegate to the corresponding callback method of the controller.
        """
        print("window: note_selected")
        if self.note_selection_enabled:
            treeModel, treeIter = selection.get_selected()
            if treeIter is not None:
                print("    selected note_id = " + str(treeModel[treeIter][1]))
                self.controller.note_selected(self.cur_db,
                                              treeModel[treeIter][1])

    def set_notes(self, notes):
        """
        Set the list of notes to be shown in the second column of the app.
        """
        # clear notes before adding new notes
        self.note_store.clear()
        if notes is None:
            return
        for note in notes:
            self.note_store.append([note[1], note[0]])

    def apply_note_to_webview(self, note_as_html):
        """
        Set the html to be displayed by the webview widget.
        """
        self.webview.apply_html(note_as_html)

    def apply_note_to_source_view(self, note_as_markdown):
        """
        Set the not to be displayed by the gtk sourceview widget.
        """
        self.buffer_md.set_text(note_as_markdown)

    def display_html(self):
        """
        Method to switch the displayed widget of the stack to the
        webview widget used for displaying a note as html.
        But only if it is not already the visible widget.
        """
        if self.stack.get_visible_child_name() != "display":
            self.stack.set_visible_child(self.webview)

    def display_edit(self):
        """
        Method to switch the displayed widget of the stack to the
        gtk sourceview widget used for editing a note. But only if it is not
        already the visible widget.
        """
        if self.stack.get_visible_child_name() != "edit":
            self.stack.set_visible_child(self.sourceview)

    def webview_context_menu_callback(self, webview, context_menu,
                                      hit_result_event, event):
        """
        Callback for the context menu of the webview that displays the markdown
        as html. Instead on displaying the context menu in right click, the
        app will switch to edit mode and use a gtk sourceview to edit the
        note.
        """

        # avoid the context menu to be shown
        # webview.hide_context_menu(context_menu)

        selection = self.notes_listview.get_selection()
        (treemodel, treeiter) = selection.get_selected()
        # only perform switch to edit mode when there is a note selected
        if treeiter is not None:
            # switch to edit mote (gtk sourceview widget)
            self.display_edit()

        return True

    def sourceview_context_menu_callback(self, sourceview, context_menu):
        """
        Callback for the opening of the context menu of the gtk sourceview.
        A context menu is not wanted. Instead on right click on the sourceview
        it should switch to the html display of the markdown note.
        """

        # avoid context menu to be shown
        context_menu.detach()
        context_menu.popdown()

        start_iter = self.buffer_md.get_start_iter()
        end_iter = self.buffer_md.get_end_iter()
        text = self.buffer_md.get_text(start_iter, end_iter, True)
        self.controller.update_note(text)

        # switch to html is performed by controller so no need
        # to call it here

    def about_button_clicked(self, button):
        """
        Callback for the "about" button.
        """
        dialog = AboutDialog(self, self.config)
        dialog.run()
        dialog.destroy()

    def settings_button_clicked(self, button):
        """
        Callback for the "settings" button.
        """
        dialog = SettingsDialog(self, self.config)
        dialog.run()
        dialog.destroy()

    def add_new_db_clicked(self, button):
        """
        Callback for the "add new database" button.
        """
        dialog = AddDatabaseDialog(self)
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            db_name = dialog.get_db_name()
            self.controller.add_new_database(db_name)

        dialog.destroy()

    def add_new_note_clicked(self, button):
        """
        Callback for the "add new note" button.
        """
        self.controller.add_new_note()

    def refresh_categories(self, db_name, categories):
        """
        Refresh displayed categories for a specific db with the given
        categories.
        """
        db = self.db_views[db_name]
        # apply new categories
        db.apply_categories(categories)

    def handle_note_mouse_click(self, treeview, event):
        """
        Handle mouse clicks in the notes listview.
        Used to show a context menu
        """
        if event.button == 3:
            selection = treeview.get_selection()
            (treemodel, treeiter) = selection.get_selected()
            if treeiter is not None:
                print(treemodel[treeiter][1])
                menu = NoteContextMenu(self,
                                       self.controller,
                                       treemodel[treeiter][1])
                menu.popup(None, None, None, None, event.button, event.time)

    def programmatic_select_note(self, note_id):
        """
        select a note programmaticaly instead of clicking on it.
        """
        selection = self.notes_listview.get_selection()
        iter = self.note_store.get_iter_first()
        while iter is not None:
            note_col_value = self.note_store.get_value(iter, 1)
            if note_id == note_col_value:
                selection.select_iter(iter)
                return
            iter = self.note_store.iter_next(iter)
