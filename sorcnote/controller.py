#
#               This file is part of SorcNote.
#
#         Copyright (C) 2017 sorcerersr@mailbox.org
#
# SorcNote is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SorcNote is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#

from dialogs import show_error_dialog
from pathlib import Path
from window import MyWindow
from config import Key
import sqlite3
from datetime import datetime, date  # noqa : F401
import markdown2
import re


class Category(object):
    """
    Entity for a category.
    """

    def __init__(self, cat_row):
        self.id = cat_row[0]
        self.name = cat_row[1]
        self.image = cat_row[2]
        self.created_at = cat_row[3]


class Controller(object):
    """
    Backend for the UI to perform non UI logic.
    """

    def __init__(self, commons, config):
        """
        Constructor.
        """
        self.config = config
        self.commons = commons
        self.selected_cat_id = None
        self.db_name = None
        self.selected_note_id = None

    def show_gui(self):
        """
        Show the gui.
        Instantiate the window and load the data to be displayed.
        """
        # create window and run main loop
        self.win = MyWindow(self.commons, self.config, self)

        self.load_databases()

        for db in self.dbs.keys():
            categories = self.load_categories(db)
            self.win.append_category(db, categories)

        self.win.run_main_loop()

    def load_databases(self):
        """
        Load the databases.
        """
        self.dbs = {}
        databases = self.config[Key.DATABASES]
        # sort by order
        databases = sorted(databases, key=lambda db: db["order"])
        for database in databases:
            print(database)
            # check if database file exists:
            path_to_file = self.config[Key.DATA_DIR] + '/' + database["file"]
            if not Path(path_to_file).is_file():
                con = self.__db_connect(path_to_file)
                self.dbs[database["name"]] = con
                self.create_database(con)
            else:
                con = self.__db_connect(path_to_file)
                self.dbs[database["name"]] = con

    def __db_connect(self, path_to_db_file):
        """
        Convenience method to get a connection object for a given sqlite3
        databse file.
        """
        return sqlite3.connect(path_to_db_file,
                               detect_types=sqlite3.PARSE_DECLTYPES |
                               sqlite3.PARSE_COLNAMES)

    def load_categories(self, dbname):
        """
        Get all categories from the categories table of the given database
        name.
        """
        print("load categories")
        cursor = self.dbs[dbname].cursor()
        cursor.execute("select id, name, image, created_at from categories")
        result = cursor.fetchall()
        categories = []
        for row in result:
            category = Category(row)
            categories.append(category)
        return categories

    def load_notes(self, dbname, category_id):
        """
        Load all notes from a database with the given name and for the given
        category id.

        Just loads the id and name. The real note (the markdown) is only
        loaded when a specific note is selected and should be edited/displayed.
        """
        print("load notes")
        cursor = self.dbs[dbname].cursor()
        cursor.execute("select id, name from notes where category=?",
                       (category_id,))
        result = cursor.fetchall()
        return result

    def load_note(self, dbname, note_id):
        """
        Load a note from the database with the given name and with the given
        id of the note.

        """
        print("load note")
        cursor = self.dbs[dbname].cursor()
        cursor.execute("select id, name, note from notes where id=?",
                       (note_id,))
        result = cursor.fetchall()
        return result

    def create_database(self, connection):
        """
        Create the table structure for a new database.
        """
        cursor = connection.cursor()
        cursor.execute("CREATE TABLE Categories " +
                       "(id INTEGER PRIMARY KEY, name TEXT, " +
                       "image BLOB, created_at TIMESTAMP)")

        cursor.execute(
            "CREATE TABLE Notes (id INTEGER PRIMARY KEY, category INTEGER, " +
            "name TEXT, note TEXT, created_at TIMESTAMP, " +
            "FOREIGN KEY(category) REFERENCES Categories(id))")

    def category_selected(self, db_name, category_id):
        """
        Callback for the selection of a category.
        """
        print("category_selected")
        print(db_name, " ", category_id)
        # deselect selections from other dbs
        self.win.deselect_others(db_name)
        notes = self.load_notes(db_name, category_id)
        self.win.set_notes(notes)

        self.db_name = db_name
        self.selected_cat_id = category_id
        self.win.add_note_button.set_sensitive(True)

        # reset note display to not show any note
        # the user must first select a new note to display after switching
        # to a other category
        self.selected_note_id = None
        self.win.apply_note_to_webview("Please select a note to display")
        self.win.apply_note_to_source_view("")

    def note_selected(self, db_name, note_id):
        """
        Callback for the selection of a note.
        """
        print("note_selected for id = " + str(note_id))
        note = self.load_note(db_name, note_id)
        html = markdown2.markdown(
            note[0][2], extras=["fenced-code-blocks", "tables"])
        html = self.extend_html(html)
        self.win.apply_note_to_webview(html)
        self.win.apply_note_to_source_view(note[0][2])
        self.win.display_html()
        self.selected_note_id = note_id

    def extend_html(self, html):
        """
        Extend the generated html with css styles etc.
        """
        print("extend_html")

        theme_name = self.config[Key.SYNTAX_THEME]
        css_path = self.config.app_root + '/css/' + theme_name + '.css'

        head = "<!DOCTYPE html><html><head><style>"
        with open(css_path, "r") as file:
            css = file.read()
            head += css
        head += "</style></head><body>"

        foot = "</body></html>"

        html = head + html + foot

        return html

    def add_new_database(self, db_name):
        """
        Adds a new database.
        """
        databases = self.config[Key.DATABASES]
        file = db_name + '.db'
        path_to_file = self.config[Key.DATA_DIR] + '/' + file

        if db_name in self.dbs:
            show_error_dialog(self.win, "Database already exists",
                              "Database with that name "
                              + "already exists. Database not created")
            # return from method without creating the db
            return
        elif Path(path_to_file).is_file():
            show_error_dialog(self.win, "Database file already exists",
                              "Database file with that name "
                              + "already exists. Database not created")
            # return from method without creating the db
            return
        con = self.__db_connect(path_to_file)
        self.dbs[db_name] = con
        self.create_database(con)

        new_db = {"name": db_name, "file": file, "order": len(databases)}
        databases.append(new_db)

        self.config.write_config()
        # refresh ui
        categories = self.load_categories(db_name)
        self.win.append_category(db_name, categories)
        self.win.show_all()

    def add_new_category(self, db_name, cat_name, cat_image_path):
        """
        Adds a new category to a database.
        """
        con = self.dbs[db_name]

        if cat_image_path:
            with open(cat_image_path, 'rb') as input_file:
                image_blob = input_file.read()
                sql = '''INSERT INTO categories
                    (image, name, created_at)
                    VALUES(?, ?, datetime('now'));'''
                con.execute(sql, [sqlite3.Binary(image_blob),
                                  cat_name])
        else:
            sql = '''INSERT INTO categories
                    (name, created_at)
                    VALUES(?, datetime('now'));'''
            con.execute(sql, [cat_name])

        con.commit()
        categories = self.load_categories(db_name)
        self.win.refresh_categories(db_name, categories)

    def delete_category(self, db_name, cat_id):
        """
        Delete a category.
        """
        print("delete_category")
        con = self.dbs[db_name]
        cursor = con.cursor()
        sql = '''DELETE FROM categories
                    WHERE id=?;'''
        cursor.execute(sql, [cat_id])

        # delete notes associated with the category
        sql = '''DELETE FROM notes
                    WHERE category=?;'''
        cursor.execute(sql, [cat_id])

        con.commit()

        # category is deleted but don't select any other
        # reset selected db, cat and disable add_note button
        self.db_name = None
        self.selected_cat_id = None
        self.win.add_note_button.set_sensitive(False)
        # don't display notes until a new category is selected
        self.win.set_notes(None)
        self.win.apply_note_to_webview('')
        self.win.display_html()

        # refresh ui
        categories = self.load_categories(db_name)
        self.win.refresh_categories(db_name, categories)

    def add_new_note(self):
        """
        Adds a new note.
        """
        if (not self.db_name) or (not self.selected_cat_id):
            print("no category selected")
            return
        con = self.dbs[self.db_name]
        cursor = con.cursor()
        sql = '''INSERT INTO notes
                    (category, name, note, created_at)
                    VALUES(?, "unnamed", "", datetime('now'));'''
        cursor.execute(sql, [self.selected_cat_id])
        con.commit()

        # reload notes view to show the new note
        notes = self.load_notes(self.db_name, self.selected_cat_id)
        self.win.set_notes(notes)

        # select the new note and switch directly to edit mode
        self.note_selected(self.db_name, cursor.lastrowid)
        self.win.display_edit()

    def update_note(self, markdown):
        """
        Updates a note.
        """
        print("update_note")
        self.win.note_selection_enabled = False
        title = self.get_title_from_markdown(markdown)
        note_id = self.selected_note_id
        print("   update note with id = " + str(note_id))
        con = self.dbs[self.db_name]
        sql = ''' UPDATE notes
              SET name = ? ,
                  note = ?
              WHERE id = ? '''
        cur = con.cursor()
        cur.execute(sql, [title, markdown, note_id])
        con.commit()

        # reload notes view to show posible title change
        notes = self.load_notes(self.db_name, self.selected_cat_id)
        self.win.set_notes(notes)
        # reload ui, generate html and display html presentation of markdown
        self.win.note_selection_enabled = True
        self.win.programmatic_select_note(note_id)
        print("   finished updating note with id = " + str(note_id))

    def get_title_from_markdown(self, markdown):
        """
        Helper method to extract a title from the markdown.
        Expects the first line to be the title.
        Can be a markdown heading so can start with any number of #
        The # will be ignored.
        """
        # handle empty markdown
        if not markdown:
            return 'unnamed'

        # only look at first line
        first_line = markdown.split('\n', 1)[0]
        # handle markdown heading
        match = re.search('(#* *)(.{0,50})', first_line)
        if match:
            # second matched group is the heading
            return match.group(2)
        else:
            return 'unnamed'

    def delete_note(self, note_id):
        """
        Delete a note by id.
        """
        print("delete note")
        print(note_id)

        con = self.dbs[self.db_name]
        cursor = con.cursor()
        sql = '''DELETE FROM notes
                    WHERE id=?;'''
        cursor.execute(sql, [note_id])

        con.commit()

        # reload notes view to remove deleted note from ui
        notes = self.load_notes(self.db_name, self.selected_cat_id)
        self.win.set_notes(notes)
