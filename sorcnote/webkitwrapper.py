#
#               This file is part of SorcNote.
#
#         Copyright (C) 2017 sorcerersr@mailbox.org
#
# SorcNote is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SorcNote is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#

import gi
gi.require_version('WebKit', '3.0')
from gi.repository import WebKit  # noqa: E402


class WebViewWrapper(WebKit.WebView):
    """
    Wraps a WebKit.WebView. This is done in order to have multiple wrappers
    for different versions of Gtk Webview.
    For example on linux it can be Webkit2.WebView v4.0 (for me on Arch Linux)
    but on Windows with mingw64 it is Webkit.WebView v.3.0.
    And both have a slightly different api methods.
    """

    def __init__(self, app_root):
        """
        Constructor.
        """
        WebKit.WebView.__init__(self)
        self.app_root = app_root

    def apply_zoom_level(self,  zoom_factor):
        """
        Set a zoom factor for the webview
        """
        self.set_zoom_level(zoom_factor)

    def apply_html(self, html):
        """
        Apply a html string to the webview.
        """
        self.load_html_string(html, self.app_root)
