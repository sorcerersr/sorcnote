#
#               This file is part of SorcNote.
#
#         Copyright (C) 2017 sorcerersr@mailbox.org
#
# SorcNote is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SorcNote is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#
import json


class Key(object):

    # some constants for the attribute keys
    DATA_DIR = 'data_dir'
    WINDOW_SIZE_HEIGHT = 'window_size_height'
    WINDOW_SIZE_WIDTH = 'window_size_width'
    WINDOW_MAXIMIZED = 'window_maximized'
    WINDOW_POS_X = 'window_pos_x'
    WINDOW_POS_Y = 'window_pos_y'
    DATABASES = "databases"
    SYNTAX_THEME = "syntax_theme"
    SOURCEVIEW_SCHEME = "sourceview_scheme"
    ZOOM_FACTOR = "zoom_factor"


class Config(dict):
    """
    Config class providing access to the app configuration file and its data.
    """

    def __init__(self, commons):
        """
        Constructor for this config class. Excpects the path to the config
        directory to be given as parameter.
        """
        self.config_file = commons.config_dir + '/config.json'
        print(self.config_file)
        self.load_config()
        self.app_root = commons.app_root
        self.commons = commons

    def load_config(self):
        """Load data from the config json file."""
        try:
            with open(self.config_file, "r") as file:
                self.update(json.load(file))

        except FileNotFoundError as fnfe:
            self.create_default_config()

    def write_config(self):
        """write current values to the config file."""
        with open(self.config_file, "w") as file:
            json.dump(self, file, sort_keys=True, indent=4)

    def create_default_config(self):
        """Create new default values and write them to the config file."""
        self[Key.DATA_DIR] = None
        self[Key.WINDOW_SIZE_HEIGHT] = 600
        self[Key.WINDOW_SIZE_WIDTH] = 400
        self[Key.WINDOW_MAXIMIZED] = False
        self[Key.WINDOW_POS_X] = 0
        self[Key.WINDOW_POS_Y] = 0
        self[Key.DATABASES] = []
        self[Key.SYNTAX_THEME] = "monokai"
        self[Key.SOURCEVIEW_SCHEME] = "monokai-extended"
        self[Key.ZOOM_FACTOR] = 1.0

        self.write_config()
