#
#               This file is part of SorcNote.
#
#         Copyright (C) 2017 sorcerersr@mailbox.org
#
# SorcNote is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SorcNote is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk  # noqa: E402
from dialogs import ConfirmDialog  # noqa: E402


class CategoryContextMenu(Gtk.Menu):
    """
    Context menu of categories treeview selection.
    """

    def __init__(self, window, controller, db_name, cat_id):
        """
        Constructor.
        """
        self.win = window
        Gtk.Menu.__init__(self)
        self.db_name = db_name
        self.cat_id = cat_id
        self.controller = controller
        item_delete = Gtk.MenuItem("delete")
        self.append(item_delete)
        item_edit = Gtk.MenuItem("edit")
        self.append(item_edit)
        self.show_all()
        item_delete.connect("activate", self.delete_activated)
        item_edit.connect("activate", self.edit_activated)

    def delete_activated(self, menu_item):
        """
        Callback for delete menu item clicked.
        """
        dialog = ConfirmDialog(self.win, "Delete category", "The selected "
                               + "category will be deleted (including "
                               + "associated notes) when you "
                               + "click the ok button!")
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            # reset selection of categories
            db = self.win.db_views[self.db_name]
            db.reset_selection()
            # delegate delete call to controller
            self.controller.delete_category(self.db_name, self.cat_id)

        dialog.destroy()

    def edit_activated(self, menu_item):
        """
        Callback for edit menu item clicked.
        """
        # ToDo : implement
        print("edit clicked")


class NoteContextMenu(Gtk.Menu):
    """
    Context menu of notes listview selection.
    """

    def __init__(self, window, controller, note_id):
        """
        Constructor.
        """
        self.win = window
        Gtk.Menu.__init__(self)
        self.note_id = note_id
        self.controller = controller
        item_delete = Gtk.MenuItem("delete")
        self.append(item_delete)
        self.show_all()
        item_delete.connect("activate", self.delete_activated)

    def delete_activated(self, menu_item):
        """
        Callback for delete menu item clicked.
        """
        dialog = ConfirmDialog(self.win, "Delete note", "The selected "
                               + "note will be deleted when you "
                               + "click the ok button!")
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            # delegate delete call to controller
            self.controller.delete_note(self.note_id)

        dialog.destroy()
