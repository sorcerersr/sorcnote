#!/bin/env python3
#
#               This file is part of SorcNote.
#
#         Copyright (C) 2017 sorcerersr@mailbox.org
#
# SorcNote is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SorcNote is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#
"""
The entry point for starting the application
"""

from commons import Commons
from config import Config, Key
from controller import Controller

# init configuration
commons = Commons()

# init configuration
config = Config(commons)
# get data dir from config

controller = Controller(commons, config)

data_dir = config[Key.DATA_DIR]
if not data_dir:
    # create data dir
    data_dir = commons.prepare_path(commons.default_data_dir)
    config[Key.DATA_DIR] = data_dir
    config.write_config()

controller.show_gui()
