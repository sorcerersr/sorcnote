#
#               This file is part of SorcNote.
#
#         Copyright (C) 2017 sorcerersr@mailbox.org
#
# SorcNote is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SorcNote is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk  # noqa: E402


class AddDatabaseDialog(Gtk.Dialog):
    """
    A dialog to enter the needed data for creating a new database.
    """

    def __init__(self, parent):
        """
        Constructor.
        """
        Gtk.Dialog.__init__(self, "Add new database", parent, 0,
                            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                             Gtk.STOCK_OK, Gtk.ResponseType.OK))

        self.set_default_size(150, 100)

        label = Gtk.Label(
            "Please enter the name for the new database to create")

        box = self.get_content_area()

        box.add(label)

        self.entry = Gtk.Entry()

        box.add(self.entry)

        self.show_all()

    def get_db_name(self):
        """
        Get the entered name for the database to create.
        """
        return self.entry.get_text()


def show_error_dialog(parent, title, text):
    """
    Display a error using a MessageDialog
    """
    dialog = Gtk.MessageDialog(parent, 0, Gtk.MessageType.ERROR,
                               Gtk.ButtonsType.CANCEL, title)
    dialog.format_secondary_text(text)

    dialog.run()

    dialog.destroy()


class AddCategoryDialog(Gtk.Dialog):
    """
    A dialog to enter the needed data for creating a new category.
    """

    def __init__(self, parent):
        """
        Constructor.
        """
        Gtk.Dialog.__init__(self, "Add new category", parent, 0,
                            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                             Gtk.STOCK_OK, Gtk.ResponseType.OK))

        self.set_default_size(150, 100)

        label = Gtk.Label(
            "Please enter the name for the new category to create")

        box = self.get_content_area()

        box.add(label)

        self.entry_name = Gtk.Entry()

        box.add(self.entry_name)

        self.file_name = Gtk.Entry()
        self.file_name.set_editable(False)

        label2 = Gtk.Label(
            "Select file to use as category image")

        box.add(label2)

        file_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        file_box.pack_start(self.file_name, True, True, 0)

        button = Gtk.Button(None, image=Gtk.Image(stock=Gtk.STOCK_FILE))
        button.connect("clicked", self.file_select)
        file_box.pack_start(button, False, True, 0)

        box.add(file_box)

        self.show_all()

    def file_select(self, button):
        dialog = Gtk.FileChooserDialog("Please choose a file", self,
                                       Gtk.FileChooserAction.OPEN,
                                       (Gtk.STOCK_CANCEL,
                                        Gtk.ResponseType.CANCEL,
                                        "Select", Gtk.ResponseType.OK))
        dialog.set_default_size(800, 400)
        filter = Gtk.FileFilter()
        filter.add_pattern('*.png')
        filter.set_name('PNG images')
        dialog.add_filter(filter)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            print("file selected: " + dialog.get_filename())
            self.file_name.set_text(dialog.get_filename())
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()

    def get_cat_name(self):
        """
        Get the entered name for the database to create.
        """
        return self.entry_name.get_text()

    def get_cat_image(self):
        """
        Get the pass of the selected file.
        """
        return self.file_name.get_text()


class ConfirmDialog(Gtk.Dialog):
    """
    A generic dialog to confirm a operation. The title and the label
    text are passed as parameters to the constructor.
    """

    def __init__(self, parent, title_text, label_text):
        """
        Constructor.
        """
        Gtk.Dialog.__init__(self, title_text, parent, 0,
                            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                             Gtk.STOCK_OK, Gtk.ResponseType.OK))

        self.set_default_size(150, 100)

        label = Gtk.Label(label_text)

        box = self.get_content_area()

        box.add(label)

        self.show_all()


class AboutDialog(Gtk.AboutDialog):
    """
    The about dialog of the application.
    """

    def __init__(self, parent, config):
        """
        Constructor.
        """
        Gtk.AboutDialog.__init__(self, "About", parent, 0)

        self.set_authors(["Sorcerer<sorcerersr@mailbox.org>", None])

        license_path = config.app_root + '/LICENSE'
        with open(license_path, "r") as file:
            license = file.read()
            self.set_license(license)

        self.set_program_name(config.commons.app_name)
        self.set_comments(config.commons.app_desc)
        self.set_version(config.commons.app_ver)
        self.set_copyright("(c) 2017")

        # logo = Pixbuf.new_from_file(config.app_root + '/logo/logo.png')

        self.set_website("https://gitlab.com/sorcerersr/sorcnote")
        self.set_website_label("SorcNote on GitLab.com")

        self.set_default_size(150, 100)

        self.show_all()


class SettingsDialog(Gtk.Dialog):
    """
    A dialog to change settings of SorcNote.
    """

    def __init__(self, parent, config):
        """
        Constructor.
        """
        Gtk.Dialog.__init__(self, "Settings", parent, 0,
                            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                             Gtk.STOCK_OK, Gtk.ResponseType.OK))

        self.set_default_size(400, 100)

        label = Gtk.Label(
            "To be implemented...")

        box = self.get_content_area()

        box.add(label)

        label = Gtk.Label(
            "For now edit the config file directly.")

        box.add(label)

        label = Gtk.Label(
            "You can find the config file here:")

        box.add(label)

        entry = Gtk.Entry()
        entry.set_text(config.config_file)
        entry.set_editable(False)

        box.add(entry)

        self.show_all()
