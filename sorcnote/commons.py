#
#               This file is part of SorcNote.
#
#         Copyright (C) 2017 sorcerersr@mailbox.org
#
# SorcNote is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SorcNote is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#
from appdirs import user_config_dir, user_data_dir
import os


class Commons(object):
    """Provide common data like path to the app directory, configuration."""

    app_name = 'SorcNote'
    app_ver = '0.0.1'
    app_desc = 'a markdown note taking application'
    author = 'sorcerer'

    def __init__(self):
        """Constructor."""
        self.package_dir = self.__determine_path()
        self.app_root = os.path.normpath(os.path.join(self.package_dir,
                                                      '../'))
        # resource dirs
        print(self.package_dir + '   - (package_dir)')
        print(self.app_root + '   - (app_root)')

        self.config_dir = os.path.expanduser(
            user_config_dir(self.app_name.lower()))

        self.config_dir = self.prepare_path(self.config_dir)
        print(self.config_dir + '   - (config_dir)')

        self.default_data_dir = os.path.expanduser(
            user_data_dir(self.app_name.lower(), self.author))

        print(self.default_data_dir + '   - (default_data_dir)')

    def __determine_path(self):
        """Get path of programm location."""
        root = __file__
        if os.path.islink(root):
            root = os.path.realpath(root)
        return os.path.dirname(os.path.abspath(root))

    def prepare_path(self, path):
        """
        Prepare the path for usage.
        Create directory if it does not exist.
        """
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except OSError:
                # fallback to app root
                return os.path.join(self.package_dir, '../')
        return path
